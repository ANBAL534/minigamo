package game

import ships.Enemy
import ships.EnemyFleet
import ships.Player
import station.Deal
import text.Codex

class Gui(val playerShip: Player) {

    @Volatile var event = ""
    @Volatile var menu: List<Any> = listOf()

    fun refresh(){

        if(System.getProperty("os.name").startsWith("Windows"))
            ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor()
        else
            print("\u001b[H\u001b[2J")

        println("\n" +
                "_Ship-Status_\n" +
                "Shield Level: ${playerShip.shields} / ${playerShip.maxShields}\n" +
                "Hull Hitpoints: ${playerShip.hitpoints}\n" +
                "Weapon Base Damage: ${playerShip.damage}\n" +
                "Shield Recharge Rate: ${playerShip.shieldRecharge}\n" +
                "Weapons Shooting Rate: ${playerShip.shootingRate}\n" +
                "Fuel: ${playerShip.fuel} / ${playerShip.maxFuel}\n" +
                "Points: ${playerShip.points}\n" +
                "\n" +
                "_Event_\n" +
                "$event\n" +
                "\n" +
                "_Menu_\n" +
                "${menu.toMenu()}\n" +
                "\n" +
                "\n" +
                "_Integrated-Decrypter_\n" +
                if(playerShip.decryptingCodex != null)
                            "Decrypted Size: ${(playerShip.decryptingCodex as Codex?)?.decoded!!.length}/${(playerShip.decryptingCodex as Codex?)?.size!!-1}\n" +
                            "Codex Tier: ${(playerShip.decryptingCodex as Codex?)?.tier ?: "?"}\n" +
                            "Codex Points: ${(playerShip.decryptingCodex as Codex?)?.points ?: "?"}\n\n" +
                            "${(playerShip.decryptingCodex as Codex?)?.decoded ?: ""}\n"
                else
                    "Offline")

    }

    private fun List<Any>.toMenu(): String{

        var menuString = ""
        var index = 1
        for (any in menu){

            when(any){

                is String -> menuString += "$index.- $any\n"
                is Planet -> menuString += "$index.- ${any.name}  (Will use ${any.fuel} fuel units)\n"
                is Deal -> menuString += "$index.- ${any.text}\n"
                is Enemy -> menuString += "$index.- ${any.text}\n"

            }

            index++
        }
        return menuString.trim()

    }

    companion object {

        lateinit var currentGui: Gui

    }

}