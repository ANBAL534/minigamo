package game

import ships.Player
import station.Station
import text.Book
import text.Codex
import java.util.*

data class Planet (val name: String, val player: Player, val gui: Gui){

    val codex: Codex? = if(Random().nextInt(10) > 5) Book.getABook().getRandomCodex() else null
    val fuel = if(codex == null) Random().nextInt(500)+1 else (codex.basePoints.toFloat()*(Random().nextFloat()+0.5)).toInt()
    val station = if(codex == null) if(Random().nextInt(3) == 2) Station(player, gui) else null else null

}