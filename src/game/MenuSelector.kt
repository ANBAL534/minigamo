package game

import com.beust.klaxon.Klaxon
import javafx.scene.layout.VBox
import ships.Player
import tornadofx.*
import java.io.File
import java.lang.Thread.sleep
import kotlin.concurrent.thread

class MyApp: App(MenuSelector::class)

class MenuSelector : View("Menu") {
    override val root = VBox()

    init{

        root.setMinSize(200.0, 200.0)

        for (i in 1..10)
            root += button("    $i    ") {
                action {

                    val selection = i
                    Logic.selectedOption = selection

                }
            }

        root += button("Jump Out!!"){
            action {

                thread (start = true, isDaemon = true){
                    if(Player.currentPlayer.inCombat){

                        Player.currentPlayer.decryptingCodex = null

                        Gui.currentGui.event = "Emergency Extraction!  Unlinking the Integrated Decryptor"
                        sleep(1000)
                        Gui.currentGui.event += "."
                        sleep(1000)
                        Gui.currentGui.event += "."
                        sleep(1000)
                        Gui.currentGui.event += "."
                        sleep(1000)

                        Gui.currentGui.event = "Charging Jump Drive Coils"
                        sleep(1000)
                        Gui.currentGui.event += "."
                        sleep(1000)
                        Gui.currentGui.event += "."
                        sleep(1000)
                        Gui.currentGui.event += "."
                        sleep(1000)
                        Gui.currentGui.event += "."
                        sleep(1000)
                        Gui.currentGui.event += "."
                        sleep(500)


                        Gui.currentGui.event = "Jumping"

                        sleep(1000)

                        Player.currentPlayer.extracting = true

                    }else{

                        val current = Gui.currentGui.event
                        Gui.currentGui.event = "Not in an emergency situation"
                        sleep(2000)
                        Gui.currentGui.event = current

                    }
                }

            }
        }

        root += button("Save Game"){
            action {

                if(!Player.currentPlayer.inCombat)
                    File("player.sav").writeText(Klaxon().toJsonString(Player.currentPlayer))

            }
        }

        root += button("Bot Mode"){
            action {

                Logic.botMode = !Logic.botMode
                val current = Gui.currentGui.event
                Gui.currentGui.event = "Bot mode is now ${if (Logic.botMode) "ON" else "OFF"}"
                sleep(2000)
                Gui.currentGui.event = current

            }
        }

    }

}
