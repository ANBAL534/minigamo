package game

import com.beust.klaxon.Klaxon
import ships.EnemyFleet
import ships.Player
import text.Codex
import java.io.File
import java.lang.Thread.sleep
import java.util.*
import kotlin.concurrent.thread
import kotlin.system.exitProcess

class Logic(val player: Player, val gui: Gui) {

    var currentIterations = 0

    fun startGame() = thread(start = true, isDaemon = false){

        gui.event = "Welcome commander.\n" +
                "Now you are in charge of a decrypter-class combat ship after the former commander died in the last decryption...\n" +
                "Your mission is going out there and try to find an decrypt all the Codex fragments you find.\n" +
                "That is dangerous because those are usually defended by automatons that are activated by the Integrated Decryptor your ship has onboard.\n" +
                "\n" +
                "Yur ship is also equipped with me, a powerful AI that will guide and give you at each moment the best decision you as a commander may take for the ship to keep alive and searching for more Codex fragments.\n" +
                "\n" +
                "Get ready for jumping commander...\n"
        gui.menu = listOf("Continue")

        if(Logic.botMode){
            sleep(1000)
        }else{
            while (selectMenuOption() != 1){sleep(10)}
        }

        gui.menu = listOf()

        while (true){

            // Autosave
            if(!Player.currentPlayer.inCombat && currentIterations > 20){
                File("player.sav").writeText(Klaxon().toJsonString(Player.currentPlayer))
                currentIterations = 0
            }
            currentIterations++

            gui.event = "I have found using the ship scanner a series of Planets within this ship's Warp Drive Jump range that may have a good possibility of holding a Codex Fragment.\n" +
                    "You, as a commander, should choose one on the list below."

            val menuList = generateJumpLocs()
            gui.menu = menuList

            var enoughFuel = false
            var selectedOption: Int
            lateinit var selectedPlanet: Planet
            while (!enoughFuel){

                try{
                    selectedOption = if(!Logic.botMode)
                        selectMenuOption()
                    else
                        Random().nextInt(10)+1
                    selectedPlanet = menuList[selectedOption-1]
                    gui.event = "Planet selected -> ${selectedPlanet.name}\n" +
                            (if(selectedPlanet.fuel <= player.fuel) "" else "Not enough fuel")

                    sleep(1500)

                    if(selectedPlanet.fuel <= player.fuel)
                        enoughFuel = true
                }catch (ignored: Exception){}

            }

            gui.menu = listOf()

            gui.event = "Charging Jump Drive Coils"
            sleep(1000)
            gui.event += "."
            sleep(1000)
            gui.event += "."
            sleep(1000)
            gui.event += "."
            sleep(1000)
            gui.event += "."
            sleep(1000)
            gui.event += "."
            sleep(500)


            gui.event = "Jumping"

            for (i in 1..selectedPlanet.fuel){
                sleep(20)
                player.fuel--
            }

            gui.event = "We have arrived to destination...\n" +
                    "It seems that we hadn't received any important damage during the Jump."

            sleep(2000)

            gui.event = "Scanning planet's surface in search for a Codex"
            sleep(1000)
            gui.event += "."
            sleep(1000)
            gui.event += "."
            sleep(1000)
            gui.event += "."
            sleep(500)

            gui.event = if(selectedPlanet.codex == null) "No Codex found on this Planet"
                    else "Codex Found!\n" +
                    "Codex Size: ${selectedPlanet.codex!!.size}\n\n" +
                    "Do you want to start decrypting it?"

            gui.menu = if(selectedPlanet.codex == null) listOf("Continue") else listOf("Yes. Start decrypting the found Codex", "No. Seems like a trap, we should keep going")

            if(selectedPlanet.codex == null){

                if(Logic.botMode){
                    sleep(1000)
                }else{
                    while (selectMenuOption() != 1){sleep(10)}
                }
                gui.menu = listOf()

                // See if we found a Station
                if(selectedPlanet.station != null){

                    gui.event = "There is a Station in orbit near, do you want to dock and ask for some deals?"
                    gui.menu = listOf("Yes. Query some deals to the station database", "No. We must continue our journey")
                    if((if(Logic.botMode) 2 else selectMenuOption()) == 1){

                        gui.menu = listOf()

                        gui.event = "Docking"
                        sleep(1000)
                        gui.event += "."
                        sleep(1000)
                        gui.event += "."
                        sleep(1000)
                        gui.event += "."
                        sleep(500)

                        gui.event = "These are some deals we found in the Station database. You can only pick one.\n" +
                                "Select one from the menu or press 10 to continue our Journey."
                        gui.menu = selectedPlanet.station!!.dealList

                        try{
                            selectedPlanet.station!!.dealList[selectMenuOption()].execute()
                        }catch(ignored: Exception){}
                        gui.menu = listOf()

                    }

                }

                gui.event = "Searching for new Planets"
                sleep(1000)
                gui.event += "."
                sleep(1000)
                gui.event += "."
                sleep(1000)
                gui.event += "."
                sleep(500)

                continue

            }else{

                val selection = if(Logic.botMode) Random().nextInt(1)+1 else selectMenuOption()
                gui.menu = listOf()
                if(selection == 2){

                    gui.event = "Searching for new Planets"
                    sleep(1000)
                    gui.event += "."
                    sleep(1000)
                    gui.event += "."
                    sleep(1000)
                    gui.event += "."
                    sleep(500)

                    continue

                }else{

                    player.extracting = false

                    gui.event = "Decrypting found Codex..."

                    player.decryptingCodex = selectedPlanet.codex
                    player.startRecharge()
                    player.inCombat = true

                    // decryption
                    thread(start = true, isDaemon = true){

                        try{
                            while (!(player.decryptingCodex as Codex?)?.isDecoded!!){

                                (player.decryptingCodex as Codex?)!!.tryDecode()
                                sleep(20)

                            }
                        }catch (ignored: NullPointerException){}

                    }

                    // combat
                    while ((player.decryptingCodex as Codex?)?.isDecoded == false && player.hitpoints > 0 && !player.extracting){

                        val fleet = EnemyFleet.generateEnemyFleet(player, (player.decryptingCodex as Codex?)!!.tier)
                        if(fleet != null){

                            gui.event = "Action Stations! Our integrated decryptor has awaken the automatons!\n"

                            thread(start = true, isDaemon = true){
                                while (fleet.inCombat && player.hitpoints > 0 && !player.extracting){
                                    gui.menu = fleet.ships
                                    sleep(100)
                                }
                            }

                            thread(start = true, isDaemon = true){

                                while (fleet.inCombat && player.hitpoints > 0 && !player.extracting){
                                    if(Logic.botMode){
                                        fleet.selectTarget(Random().nextInt(10)+1)
                                        sleep(3000)
                                    }else{
                                        fleet.selectTarget(selectMenuOption()-1)
                                        sleep(100)
                                    }
                                }

                            }

                            fleet.receiveDamage()
                            fleet.startCombat()

                            // Wait loop until combat finishes
                            while (fleet.inCombat && player.hitpoints > 0 && !player.extracting)
                                sleep(100)

                        }else{

                            sleep(10000)

                        }

                    }

                    player.stopRecharge()
                    player.inCombat = false

                    // Point earning for decoding
                    if(!player.extracting)
                        player.points += (player.decryptingCodex as Codex?)!!.points

                    if(player.hitpoints <= 0){
                        println("U DEDEAD")
                        File("player.sav").delete()
                        exitProcess(0)
                    }

                }

            }

        }

    }

    fun generateJumpLocs(): List<Planet>{

        val numberOfPlanets = Random().nextInt(10)+1
        val list = mutableListOf<Planet>()

        for (i in 1..numberOfPlanets){
            list.add(Planet("${(Random().nextInt(90-65)+65).toChar()}${(Random().nextInt(90-65)+65).toChar()}${(Random().nextInt(90-65)+65).toChar()}-${(Random().nextInt(90-65)+65).toChar()}${(Random().nextInt(90-65)+65).toChar()}", player, gui))
        }

        return list

    }

    fun selectMenuOption(): Int{

        selectedOption = -1
        while(selectedOption < 1){sleep(10)}
        val selected = selectedOption
        selectedOption = -1
        return selected

    }

    companion object {
        var selectedOption = -1
        var botMode = false
    }

}