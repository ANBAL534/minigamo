package ships

import text.Codex
import java.lang.Thread.sleep
import java.util.*
import kotlin.concurrent.thread

class EnemyFleet private constructor(@Volatile var ships: List<Enemy>, val player: Player) {

    var inCombat: Boolean = true
        get() = ships.any { it.hitpoints > 0 }

    fun receiveDamage() = thread(start = true, isDaemon = true){

        while (inCombat){

            for(enemy in ships){

                if(enemy.isTarget){

                    sleep(player.shootingRate.toLong()/10)
                    enemy.hitpoints -= (player.damage*Random().nextFloat()).toInt()/10

                }

            }

            sleep(100)

        }

    }

    fun selectTarget(target: Int){

        for (enemy in ships)
            enemy.isTarget = false

        try{
            ships[target].isTarget = true
        }catch (ignored:Exception){}

    }

    fun startCombat() = thread (start = true, isDaemon = true) {

        for (enemy in ships)
            enemy.combat()

        //Reinforces
        thread (start = true, isDaemon = true){

            while (inCombat){

                sleep((Random().nextInt(30000)+30000).toLong())
                if(ships.size < 10 && Random().nextInt(10) > 5){
                    val mutable = mutableListOf<Enemy>()
                    mutable.addAll(ships)
                    val reinforce = Enemy(player, Random().nextInt(10)+1)
                    mutable.add(reinforce)
                    reinforce.combat()
                    ships = mutable
                }

            }

        }

    }

    companion object {

        fun generateEnemyFleet(player: Player, codexTier: Int): EnemyFleet?{

            return if(Random().nextInt(10) > 5){

                val ships = mutableListOf<Enemy>()
                val shipNumber = Random().nextInt(10)+1

                for (i in 0..shipNumber)
                    ships.add(Enemy(player, codexTier))

                EnemyFleet(ships, player)

            }else{
                null
            }

        }

    }

}