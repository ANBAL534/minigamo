package ships

import java.lang.Thread.sleep
import java.util.*
import kotlin.concurrent.thread

class Enemy(val player: Player, codexTier: Int) {

    var hitpoints = ((Random().nextInt(1000)+100)*player.level*codexTier).toInt()
    val maxHitpoints = hitpoints
    val damage = (Random().nextInt(350)+50).toFloat()*player.level*codexTier
    val shootingRate = Random().nextInt(10000)+500
    var isTarget = false
    var text = "${if(hitpoints<=0)"[DEAD] " else ""}UNIT-${Random().nextInt(10)}${Random().nextInt(10)}${Random().nextInt(10)}${Random().nextInt(10)} - $hitpoints/$maxHitpoints - $damage${if(isTarget) " - TARGETED" else ""}"
        get() = "${if(hitpoints<=0)"[DEAD] " else ""}UNIT-${Random().nextInt(10)}${Random().nextInt(10)}${Random().nextInt(10)}${Random().nextInt(10)} - $hitpoints/$maxHitpoints - $damage${if(isTarget) " - TARGETED" else ""}"

    fun combat() = thread(start = true, isDaemon = true){

        sleep((Random().nextInt(10000)+1).toLong())

        while (player.hitpoints > 0 && hitpoints > 0 && !Player.currentPlayer.extracting){

            if(player.shields > 0){
                player.shields -= (damage*Random().nextFloat()*player.level).toInt()/10
            }else{
                player.hitpoints -= (damage*Random().nextFloat()*player.level).toInt()/10
            }

            sleep(shootingRate.toLong()/10)

        }

    }

}