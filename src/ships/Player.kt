package ships

import text.Codex
import java.lang.Thread.sleep
import java.util.*
import kotlin.concurrent.thread

data class Player (@Volatile var shields: Int,
                   @Volatile var hitpoints: Int,
                   @Volatile var fuel: Int,
                   var damage: Int,
                   var shieldRecharge: Int,
                   var shootingRate: Int,
                   var maxShields: Int,
                   var maxFuel: Int,
                   var level: Float,
                   var points: Int,
                   @Volatile var extracting: Boolean = false,
                   var inCombat: Boolean = false,
                   private var stopRecharge: Boolean = false
                   ){

    constructor(shields: Int,
                hitpoints: Int,
                fuel: Int,
                damage: Int,
                shieldRecharge: Int,
                shootingRate: Int,
                maxShields: Int,
                maxFuel: Int,
                level: String,  // Damn Klaxon
                points: Int,
                extracting: Boolean = false,
                inCombat: Boolean = false,
                stopRecharge: Boolean = false)
            : this(shields, hitpoints, fuel, damage, shieldRecharge, shootingRate, maxShields, maxFuel, level.toFloat(), points, extracting, inCombat, stopRecharge)

    @Volatile var decryptingCodex: Any? = null
        get() = field as Codex?

    fun startRecharge() = thread(start = true, isDaemon = true) {

        while(!stopRecharge){

            shields += (shieldRecharge.toFloat()/10.0).toInt()
            if(shields > maxShields)
                shields = maxShields
            sleep(100)

            if(shields <= 0)
                sleep(Random().nextInt(10000).toLong())

        }

        stopRecharge = false

    }

    fun stopRecharge(){
        stopRecharge = true
    }

    companion object {

        lateinit var currentPlayer: Player

    }

}