package text

import sun.awt.Mutex
import java.util.*

data class Codex (private val text: String, private val alphabet: List<Char>){

    val size: Int = text.length
    val tier: Int
    val basePoints: Int
    val points: Int
        get() {
            if(isDecoded)
                return basePoints * tier
            return -1
        }
    var isDecoded: Boolean = false
        get() = text.length == _decoded.length+1
        private set

    private val mutexDecode = Mutex()
    var decoded = ""
        get() {
            mutexDecode.lock()
            val current = field
            mutexDecode.unlock()
            return current
        }
        private set(value) {
            mutexDecode.lock()
            field = value
            mutexDecode.unlock()
        }
    private var _decoded = ""

    init {

        tier = when(Random().nextInt(100)+1){
            in 99..100 -> 10
            in 95..99 -> 6
            in 85..95 -> 4
            in 70..85 -> 3
            in 50..70 -> 2
            in 1..50 -> 1
            else -> 0
        }

        basePoints = (size.toFloat() * Random().nextFloat()).toInt()

    }

    fun tryDecode(): Boolean {

        try{
            val bruteGeneratedChar = alphabet[Random().nextInt(alphabet.size)]
            decoded = _decoded + bruteGeneratedChar

            return if(bruteGeneratedChar == text[decoded.length]){
                _decoded += bruteGeneratedChar
                true
            }else{
                false
            }

        }catch (ignored: Exception){}

        return false

    }

}