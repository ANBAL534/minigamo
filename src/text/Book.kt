package text

import java.io.File
import java.io.InputStream
import java.util.*

data class Book (val content: String){

    private var _alphabet: List<Char>? = null
    var alphabet: List<Char> = listOf()
        get() {
            var list = mutableListOf<Char>()
            if(_alphabet == null){			
				for (char in content){
					if(!list.contains(char))
						list.add(char)
				}
				_alphabet = list
			}else{
				list = _alphabet as MutableList<Char>
			}
            return list
        }
        private set

    fun getRandomCodex(): Codex{
        var origin: Int
        var end: Int
        do{
            origin = Random().nextInt(content.length-500)+1
            end = Random().nextInt(Random().nextInt(500))+origin+1
        }while (end > content.length)
        return Codex(content.subSequence(origin, end) as String, alphabet)
    }

    companion object {
        fun getABook(): Book{

            // STUB
            val inputStream: InputStream = File("bookStub.txt").inputStream()
            val inputString = inputStream.bufferedReader().use { it.readText() }
            return Book(inputString.replace("\n", " ").replace("\r", ""))

        }
    }

}