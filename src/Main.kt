import com.beust.klaxon.Klaxon
import game.Gui
import game.Logic
import game.MyApp
import javafx.application.Application
import ships.Player
import java.io.File
import java.lang.Thread.sleep
import kotlin.concurrent.thread
import kotlin.system.exitProcess

fun main(args: Array<String>){

    val savegame = File("player.sav")
    if(!savegame.exists() || savegame.readText().replace("\r\n", "\n").isEmpty())
        savegame.writeText("{\"damage\" : 400, \"decryptingCodex\" : {\"basePoints\" : 1, \"decoded\" : \" direc\", \"isDecoded\" : true, \"points\" : 3, \"size\" : 7, \"tier\" : 3}, \"extracting\" : false, \"fuel\" : 9000, \"hitpoints\" : 6000, \"inCombat\" : false, \"level\" : \"1.0\", \"maxFuel\" : 9000, \"maxShields\" : 5000, \"points\" : 0, \"shieldRecharge\" : 50, \"shields\" : 5000, \"shootingRate\" : 3000}")

    val player = Klaxon().parse<Player>(savegame.readText().replace("\r\n", "\n"))!!
    val gui = Gui(player)
    val logic = Logic(player, gui)

    thread(start = true, isDaemon = true){
        Application.launch(MyApp::class.java)
        exitProcess(0)
    }
    logic.startGame()

    Gui.currentGui = gui
    Player.currentPlayer = player

    while (true){

        gui.refresh()
        sleep(50)

    }

}