package station

import game.Gui
import ships.Player
import java.lang.Thread.sleep
import java.util.*

class Deal(val player: Player, val gui: Gui) {

    val text: String
    val price = ((Random().nextInt(1000000)+1000).toFloat()*(player.level-0.9F)).toInt()
    val given: Float
    val dealType = Random().nextInt(6)

    init{

        when(dealType){

            0 -> {
                // refill fuel
                given = (Random().nextInt(20000)+1).toFloat()
                text = "Refill $given fuel for $price points"
            }
            1 -> {
                // max shield
                given = (Random().nextFloat()+1.0F)/10.0F
                text = "Upgrade ${given*100}% max shield for $price points"
            }
            2 -> {
                // max fuel
                given = (Random().nextFloat()+1.0F)/10.0F
                text = "Upgrade ${given*100}% max fuel for $price points"
            }
            3 -> {
                // damage
                given = (Random().nextFloat()+1.0F)/10.0F
                text = "Upgrade ${given*100}% weapons damage for $price points"
            }
            4 -> {
                // hull
                given = (Random().nextInt(2000)+1).toFloat()
                text = "Repair $given hull for $price points"
            }
            5 -> {
                // shooting rate
                given = (Random().nextFloat()+1.0F)/-10.0F
                text = "Upgrade $given shooting rate for $price points"
            }
            else -> {
                given = 0.0F
                text=""
            }

        }

    }

    fun execute(){

        when(dealType){

            0 -> {
                // refill fuel
                if(player.points >= price){

                    gui.event = "Refilling..."

                    for(i in 1..given.toInt()){
                        if(player.fuel < player.maxFuel){
                            player.fuel++
                            sleep(8)
                        }else{
                            break
                        }
                    }

                    player.level += 0.01F
                    gui.event = "Done"


                }else{
                    gui.event = "Not enough available points to pick this Deal"
                }
            }
            1 -> {
                // max shield
                if(player.points >= price){

                    gui.event = "Upgrading..."

                    val finalAmmount = player.maxShields + (player.maxShields.toFloat()*given).toInt()
                    val diff = finalAmmount - player.maxShields

                    for(i in 1..diff){
                        player.maxShields++
                        sleep(5)
                    }

                    player.level += 0.1F
                    gui.event = "Done"


                }else{
                    gui.event = "Not enough available points to pick this Deal"
                }
            }
            2 -> {
                // max fuel
                if(player.points >= price){

                    gui.event = "Upgrading..."

                    val finalAmmount = player.maxFuel + (player.maxFuel.toFloat()*given).toInt()
                    val diff = finalAmmount - player.maxFuel

                    for(i in 1..diff){
                        player.maxFuel++
                        sleep(5)
                    }

                    player.level += 0.1F
                    gui.event = "Done"


                }else{
                    gui.event = "Not enough available points to pick this Deal"
                }
            }
            3 -> {
                // damage
                if(player.points >= price){

                    gui.event = "Upgrading..."

                    val finalAmmount = player.damage + (player.damage.toFloat()*given).toInt()
                    val diff = finalAmmount - player.damage

                    for(i in 1..diff){
                        player.damage++
                        sleep(5)
                    }

                    player.level += 0.1F
                    gui.event = "Done"


                }else{
                    gui.event = "Not enough available points to pick this Deal"
                }
            }
            4 -> {
                // hull
                if(player.points >= price){

                    gui.event = "Repairing..."

                    for(i in 1..given.toInt()){
                        player.hitpoints++
                        sleep(8)
                    }

                    player.level += 0.01F
                    gui.event = "Done"


                }else{
                    gui.event = "Not enough available points to pick this Deal"
                }
            }
            5 -> {
                // max fuel
                if(player.points >= price){

                    gui.event = "Upgrading..."

                    val finalAmmount = player.shootingRate + (player.shootingRate.toFloat()*given).toInt()
                    val diff = finalAmmount - player.shootingRate

                    for(i in 1..diff){
                        player.shootingRate++
                        sleep(5)
                    }

                    player.level += 0.1F
                    gui.event = "Done"


                }else{
                    gui.event = "Not enough available points to pick this Deal"
                }
            }

            else -> gui.event = "Thank you for your visit!"

        }

    }

}