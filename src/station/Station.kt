package station

import game.Gui
import ships.Player
import java.util.*

class Station(player: Player, gui: Gui) {

    val dealList: List<Deal>

    init{

        val list = mutableListOf<Deal>()
        val dealNumber = Random().nextInt(1)+1

        for (i in 0..dealNumber)
            list.add(Deal(player, gui))

        dealList = list

    }

}